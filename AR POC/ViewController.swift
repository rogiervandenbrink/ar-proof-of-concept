//
//  ViewController.swift
//  AR POC
//
//  Created by Rogier van den Brink on 09/01/2020.
//  Copyright © 2020 Incentro. All rights reserved.
//

import UIKit
import ARKit
import SceneKit
import SnapKit

class ViewController: UIViewController {

    let defaults = UserDefaults.standard
        
    @IBOutlet var sceneView: ARSCNView?
    private var planeNode: SCNNode?
    private var imageNode: SCNNode?
    var configuration = ARWorldTrackingConfiguration()
    
    private var imageName: String?
    private var authToken: String = ""
    
    @IBOutlet weak var closeButton: UIButton?
        
    var newReferenceImages: Set<ARReferenceImage> = Set<ARReferenceImage>()
    var timer: Timer! = nil
    let timerInterval = 0.5
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Handleiding"
        
        let scene = SCNScene()
        
        let sceneView = ARSCNView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.height, height: self.view.frame.size.height))
        sceneView.scene = scene
        sceneView.delegate = self
        sceneView.isUserInteractionEnabled = false
        self.view.addSubview(sceneView)
        self.sceneView = sceneView
        
        sceneView.snp.remakeConstraints { (make) in
            make.top.equalTo(self.view)
            make.right.equalTo(self.view)
            make.left.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar after calling the super
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Load reference images to look for from "AR Resources" folder
        guard let referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil) else {
            fatalError("Missing expected asset catalog resources.")
        }
        newReferenceImages = referenceImages
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        // Add previously loaded images to ARScene configuration as detectionImages
        configuration.detectionImages = referenceImages
        configuration.worldAlignment = .gravity
        
        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        
        // Run the view's session
        if let sceneView = self.sceneView {
            sceneView.session.run(configuration, options: options)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //self.resetTrackingConfiguration()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let sceneView = self.sceneView {
            // kill the view's session
            sceneView.session.pause()
            sceneView.removeFromSuperview()
            self.sceneView = nil
        }
    }
    
    override func didReceiveMemoryWarning() {
        print("memory warning")
    }
    
    @objc func resetTrackingConfiguration() {
        if let sceneView = self.sceneView {
            let configuration = ARWorldTrackingConfiguration()
            configuration.detectionImages = newReferenceImages
            let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
            sceneView.session.run(configuration, options: options)
        } else {
            
            let scene = SCNScene()
            
            let sceneView = ARSCNView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.height, height: self.view.frame.size.height))
            sceneView.scene = scene
            sceneView.delegate = self
            sceneView.isUserInteractionEnabled = false
            self.view.addSubview(sceneView)
            self.sceneView = sceneView
            
            sceneView.snp.remakeConstraints { (make) in
                make.top.equalTo(self.view)
                make.right.equalTo(self.view)
                make.left.equalTo(self.view)
                make.bottom.equalTo(self.view)
            }

            let configuration = ARWorldTrackingConfiguration()
            configuration.detectionImages = newReferenceImages
            let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
            sceneView.session.run(configuration, options: options)
        }
    }
    
    func closeScanner() {
        self.dismiss(animated: true) {
            // dismiss
        }
    }
    
    func showCableView() {
        // TODO: Show cable
    }
    
    func resetCableView() {
        // TODO: reset cable view
    }
    
    func createARReferenceImage(image: UIImage, physicalWidth: CGFloat) {
        let arImage = ARReferenceImage(image.cgImage!, orientation: CGImagePropertyOrientation.up, physicalWidth: physicalWidth)
        
        arImage.name = imageName
        
        self.newReferenceImages.insert(arImage)
    }
    
    fileprivate func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: timerInterval, target: self, selector: #selector(resetTrackingConfiguration), userInfo: nil, repeats: false)
        
    }
    
    fileprivate func invalidateTimer() {
        if self.timer != nil {
            self.timer.invalidate()
            self.timer = nil
        }
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        print("Session failed. Changing worldAlignment property.")
        print(error.localizedDescription)
        
        if let arError = error as? ARError {
            switch arError.errorCode {
            case 102:
                configuration.worldAlignment = .gravity
                restartSessionWithoutDelete()
            default:
                restartSessionWithoutDelete()
            }
        }
    }
    
    func restartSessionWithoutDelete() {
        // Restart session with a different worldAlignment - prevents bug from crashing app
        guard let sceneView = self.sceneView else { return }
        sceneView.session.pause()
        
        sceneView.session.run(configuration, options: [
            .resetTracking,
            .removeExistingAnchors])
    }
}

extension ViewController: ARSCNViewDelegate {
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let imageAnchor = anchor as? ARImageAnchor else {
            return
        }
        
        let referenceImage = imageAnchor.referenceImage
        let imageName = referenceImage.name ?? "no name"
        
        let plane = SCNPlane(width: referenceImage.physicalSize.width, height: referenceImage.physicalSize.height)
        let planeNode = SCNNode(geometry: plane)
        planeNode.opacity = 1.0
        let material = SCNMaterial()
        material.colorBufferWriteMask = []
        plane.materials = [ material ]
//        planeNode.position.y = 0.04
        planeNode.rotation = SCNVector4(x: 1, y: 0, z: 0, w: -Float.pi / 2)
        node.addChildNode(planeNode)
        
        guard let cableScene = SCNScene(named: "USB obj.obj") else { print("no cablescene"); return }
        cableScene.rootNode.scale = SCNVector3Make(Float(0.00004), Float(0.00004), Float(0.00004))
        cableScene.rootNode.position.x = 0.0015
        cableScene.rootNode.position.z = 0.0025
//        cableScene.rootNode.rotation = SCNVector4(x: 0, y: 180, z: 360, w: -Float.pi / 2)
//        cableScene.rootNode.rotation = SCNVector4(x: Float.pi, y: -Float.pi, z: Float.pi, w: Float.pi)
        cableScene.rootNode.eulerAngles = SCNVector3(x: -Float.pi / 2, y: Float.pi, z: Float.pi / 2)
//        cableScene.rootNode.rotation = SCNVector4(x: 0, y: 0, z: 0, w: 0)
        node.addChildNode(cableScene.rootNode)
        
        let moveIn = SCNAction.move(by: SCNVector3(0, -0.1, 0), duration: 2)
        let moveOut = SCNAction.move(by: SCNVector3(0,0.1,0), duration: 0.5)
        let fadeIn = SCNAction.fadeOpacity(to: 1, duration: 0.5)
        let fadeOut = SCNAction.fadeOpacity(to: 0, duration: 0.5)
        
        moveOut.timingMode = .easeInEaseOut
        moveIn.timingMode = .easeInEaseOut
        
        cableScene.rootNode.opacity = 0
        let waitAction = SCNAction.wait(duration: 0)
        let hoverSequence = SCNAction.sequence([moveOut, fadeIn, moveIn, SCNAction.wait(duration: 3), fadeOut, ])
        let loopSequence = SCNAction.repeatForever(hoverSequence)
        
        cableScene.rootNode.runAction(loopSequence)
        
        DispatchQueue.main.async {
            print("image detected: \(imageName)")
            // TODO: Actoins when router is recognized
        }
    }
}

